"use strict";

/**
 * Example of use:
 * node exportPreferences.js system-objecttype-extensions.xml preferences.xml  "Provider,Link-OrderGroove,PXConfig,Link-LivePerson,Frontend-Content,CRM3_0 Configurations,Calabrio,shoprunner,Link-RichRelevance,Frontend-Analytics,Marketing Cloud,cybersourceSettings,Backend-PFSWeb,Payment-PayPal,extole"
 */

var fs = require('fs'),
    xml2js = require('xml2js'),
    _ = require('lodash');

class CAExoportParser {

  constructor(){
    this._fileName = '';
    this._fileName2 = '';
    this._params = '';
    this._parsedObject = null;
    this._parsedObject2 = null;
    this._newObject = null;
    this._searchTree = {};
    this._contentIDs = [];
    this._attributesIDs = [];
    this._eduar = {};
    this._exportedTree = {};
  }

  async run(fileName, fileName2, params){
    this.validateInput(fileName, fileName2, params);
    this.readParameters(params);

    await this.readDefinitionsXMLfile();
    await this.processTypeExtensions(this._parsedObject.metadata['type-extension']);

    await this.readPreferencesXMLfile();
    await this.processCustomPreferences(this._parsedObject2.preferences['standard-preferences'][0]);
    await this.processCustomPreferences(this._parsedObject2.preferences['custom-preferences'][0]);

    this.exportNewXML();
  }

  validateInput(fileName, fileName2, params){
    console.log('fileName', fileName);
    console.log('fileName2', fileName2);
    console.log('params', params);

    if(!fileName || !fileName2 || !params)
      throw new Error('Please provide three parameters');

    this._fileName = fileName;
    this._fileName2 = fileName2;
    this._params = params;
  }

  async readDefinitionsXMLfile(fileName){
    return new Promise((resolve, reject) => {
      let parser = new xml2js.Parser();
      fs.readFile(__dirname + '/' + this._fileName, (err, data) => {
        parser.parseString(data, (err, result) => {
          this._parsedObject = result;
          resolve('done');
        });
      });
    });
  }

  async readPreferencesXMLfile(){
    return new Promise((resolve, reject) => {
      let parser = new xml2js.Parser();
      fs.readFile(__dirname + '/' + this._fileName2, async (err, data) => {
        parser.parseString(data, async (err, result) => {
          this._parsedObject2 = result;
          resolve('done');
        });
      });
    });
  }

  async processCustomPreferences(customPreferences){
    for (let instanceType in customPreferences){
      try {
        let preferences = customPreferences[instanceType][0].preference;
        if (preferences && Array.isArray(preferences)) {
          customPreferences[instanceType][0].preference = this.filterPreferences(preferences);
        }
      } catch (error) {
        console.error('[ERROR] processCustomPreferences: ', error)
      }
    }
  }

  filterPreferences(preferences){
    let filteredPreferences = [];
    filteredPreferences = preferences.filter(pref => {
      let preferenceId = pref.$['preference-id'];
      if (this._attributesIDs.includes(preferenceId)){
        this.storeExported(preferenceId);
        return true
      }
      return false
    });
    return filteredPreferences;
  }

  storeExported(preferenceId){
    for (let parentId in this._eduar) {
      let found = this._eduar[parentId].some(attr => attr === preferenceId);
      if (found) {
        if (!(parentId in this._exportedTree)) {
          this._exportedTree[parentId] = [];
        }
        this._exportedTree[parentId].push(preferenceId);
      }
    }
  }

  processTypeExtensions(typeExtensions){
    if (Array.isArray(typeExtensions)) {
      typeExtensions.forEach(te => {
        let typeExtensionId = te.$['type-id'];
        if (typeExtensionId === 'SitePreferences') {
          this.processTypeExtension(te);
        }
      });      
    }
  }

  processTypeExtension(typeDefinition){
    let attrGroups = typeDefinition['group-definitions'][0]['attribute-group'];
    attrGroups.forEach(ag => {
      let attributeGroupId = ag.$['group-id'];

      if (this._contentIDs.includes(attributeGroupId)) {
        this.processAttributeGroup(ag, attributeGroupId)
      }
    })
  }

  processAttributeGroup(attributeGroup, id){
    if (Array.isArray(attributeGroup.attribute)){
      attributeGroup.attribute.forEach(ag => {
        let atttributeId = ag.$['attribute-id']
        if (!(id in this._eduar)) {
          this._eduar[id] = [];
        }
        this._eduar[id].push(atttributeId);
        this._attributesIDs.push(atttributeId);
      });
    }
  }

  readParameters(){
    this._contentIDs = this._params.replace(' ', '').split(',');
    console.log('this._contentIDs', this._contentIDs);
    console.log('this._contentIDs. length: ', this._contentIDs.length);
  }

  exportNewXML(){
    let builder = new xml2js.Builder();
    let xml = builder.buildObject(this._parsedObject2);
    fs.writeFileSync('newPreferences.xml', xml);
  }
}

let obj = new CAExoportParser();
obj.run(process.argv[2], process.argv[3], process.argv[4]);
