"use strict";

/**
 * how to use:
 * node pageDesignerParser.js library.xml homepage
 */

var fs = require('fs'),
    xml2js = require('xml2js'),
    _ = require('lodash');

class CAExoportParser {

  contructor(){
    this._fileName = '';
    this._params = '';
    this._parsedObject = null;
    this._newObject = null;
    this._searchTree = {};
    this._contentIDs = [];
  }

  run(fileName, params){
    this.validateInput(fileName, params);
    this.readParameters(params);
    this.readXMLfile(fileName);
  }

  validateInput(fileName, params){
    console.log('fileName', fileName);
    console.log('params', params);

    if(!fileName || !params)
      throw new Error('Please provide both parameters');

    this._fileName = fileName;
    this._params = params;
  }

  readXMLfile(fileName){
    let parser = new xml2js.Parser();
    let that = this;
    fs.readFile(__dirname + '/' + this._fileName, function(err, data) {
      parser.parseString(data, function (err, result) {
        that._parsedObject = result;
        console.log('Original folder length: ', result.library.folder.length);
        // console.log('Folder example: ', JSON.stringify(result.library.folder, null, 2));
        console.log('Original Content length: ', result.library.content.length);
        // console.log('Content example: ', JSON.stringify(result.library.content[0], null, 2));

        that.processContent();
        that.postProcessContent();
        console.log('New Content length: ', that._parsedObject.library.content.length);

        that.exportNewXML();
      });
    });
  }

  readParameters(){
    this._contentIDs = this._params.replace(' ', '').split(',');
    console.log('this._contentIDs', this._contentIDs);
    console.log('this._contentIDs. length: ', this._contentIDs.length);
  }

  processContent(){
    this._parsedObject.library.contentTmp = [];
    this._contentIDs.forEach(contentId => this.storeContent(contentId));
  }

  postProcessContent(){
    delete this._parsedObject.library.folder;
    this._parsedObject.library.content = null;
    this._parsedObject.library.content = this._parsedObject.library.contentTmp.reverse();
    delete this._parsedObject.library.contentTmp;
  }

  storeContent(id){
    let content = this._parsedObject.library.content.find(o => o.$['content-id'] === id);
    if (content)
      this._parsedObject.library.contentTmp.push(content);
    
    this.checkChildComponents(content);
  }

  checkChildComponents(o){
    if( o['content-links'] &&
      o['content-links'] instanceof Array
      )
    {
      o['content-links'][0]['content-link'].forEach(cl => {
        let contentId = cl.$['content-id'];
        this.storeContent(contentId);
      });
    }
  }

  exportNewXML(){
    let builder = new xml2js.Builder();
    let xml = builder.buildObject(this._parsedObject);
    fs.writeFileSync('out.xml', xml);
  }
}

let obj = new CAExoportParser();
obj.run(process.argv[2], process.argv[3]);
